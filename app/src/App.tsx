import { useState } from "react";
import Slider, { Settings } from "react-slick"; // Import the Slider component
import PinInput from "react-pin-input";
import { fetchPicturesByFolder } from "./services/api.services";
import "./App.css";
import { useQuery } from "react-query";

function App() {
  const CODE_SIZE = 6;
  const DEFAULT_FOLDER = "micka";
  const [isAuthorized, setIsAuthorized] = useState<null | boolean>(null);
  const [code, setCode] = useState("");

  const { data: pictures, isLoading, refetch } = useQuery<string[]>(
    ["pictures", code],
    () => fetchPicturesByFolder(DEFAULT_FOLDER, "laptop", code),
    {
      enabled: code.length === CODE_SIZE,
      retry: false,
      onSuccess: () => setIsAuthorized(true),
      onError: () => setIsAuthorized(false),
    }
  );
  const handlePinChange = (value: string) => {
    setCode(value);
    if (value.length === CODE_SIZE) {
      refetch();
    }
  };


  const sliderSettings: Settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  };

  return (
    <div className="App">
      <header className="App-header">
        {!isAuthorized && (
          <PinInput
            length={CODE_SIZE}
            initialValue=""
            secretDelay={100}
            type="numeric"
            inputMode="number"
            style={{
              padding: "10px",
            }}
            inputStyle={{
              borderColor: isAuthorized === null ? "#282c34" : "#ff0000",
              ...(isLoading && { animation: "shimmer 1.5s infinite linear", background: "linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%)", backgroundSize: "1000px 100%" })
            }}
            onComplete={handlePinChange}
            autoSelect={true}
            regexCriteria={/^[ A-Za-z0-9_@./#&+-]*$/}
          />
        )}
        {isAuthorized && !isLoading && pictures?.length && (
          <Slider {...sliderSettings}>
            {pictures?.map((pictureUrl, index) => (
              <div className="image-container" key={index}>
                <img src={pictureUrl} className="App-picture" alt="" />
              </div>
            ))}
          </Slider>
        )}
      </header>
    </div>
  );
}

export default App;
