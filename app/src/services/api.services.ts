const BASE_URL = "https://d2c1iqh32yalnw.cloudfront.net/stage";

export const fetchPicturesByFolder = async (
  folder: string,
  platform: "laptop" | "tablet" | "phone",
  code: string
) => {
  const response = await fetch(`${BASE_URL}/pictures/${folder}`, {
    method: "GET",
    headers: {
      Authorization: code,
    },
  });
  const results = await response.json();

  return results.items[platform];
};
