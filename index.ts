import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as fs from "fs";
import * as path from "path";
import * as mimeTypes from "mime-types";

const siteBucket = new aws.s3.Bucket("memoria-ui", {
  website: {
    indexDocument: "index.html",
    errorDocument: "index.html",
  },
});

const originAccessIdentity = new aws.cloudfront.OriginAccessIdentity(
  "memoria-oai",
  {
    comment: "Access Identity for my S3 bucket.",
  }
);

const s3PolicyStatement = {
  Sid: "Grant CloudFront Origin Identity access to S3 bucket",
  Effect: "Allow",
  Principal: {
    AWS: pulumi.interpolate`arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity ${originAccessIdentity.id}`,
  },
  Action: "s3:GetObject",
  Resource: pulumi.interpolate`${siteBucket.arn}/*`,
};

const bucketPolicy = new aws.s3.BucketPolicy("memoria-ui-bucket-policy", {
  bucket: siteBucket.id,
  policy: pulumi.output([s3PolicyStatement]).apply((statements) =>
    JSON.stringify({
      Version: "2012-10-17",
      Statement: statements,
    })
  ),
});

// Upload the React app build directory to the S3 bucket
const siteDir = "app/build";

// Define the path to the React app's build directory

// Function to upload directory content recursively
function uploadDirectory(directory: string) {
  // Iterate over the items in the directory
  fs.readdirSync(directory).forEach((item) => {
    const itemPath = path.join(directory, item);
    const stats = fs.statSync(itemPath);

    if (stats.isDirectory()) {
      // Recursively upload directory
      uploadDirectory(itemPath);
    } else if (stats.isFile()) {
      // Upload file
      const fileMimeType = mimeTypes.lookup(itemPath) || undefined;
      new aws.s3.BucketObject(itemPath.replace(siteDir + "/", ""), {
        bucket: siteBucket,
        source: new pulumi.asset.FileAsset(itemPath),
        contentType: fileMimeType,
        acl: undefined,
      });
    }
  });
}

uploadDirectory(siteDir);

const distribution = new aws.cloudfront.Distribution(
  "memoria-website-distribution",
  {
    origins: [
      {
        domainName: siteBucket.bucketRegionalDomainName,
        originId: siteBucket.arn,
        s3OriginConfig: {
          originAccessIdentity:
            originAccessIdentity.cloudfrontAccessIdentityPath,
        },
      },
    ],
    enabled: true,
    isIpv6Enabled: true,
    comment: "CDN for my S3 bucket website",
    defaultRootObject: "index.html",
    defaultCacheBehavior: {
      allowedMethods: ["GET", "HEAD"],
      cachedMethods: ["GET", "HEAD"],
      targetOriginId: siteBucket.arn,
      forwardedValues: {
        queryString: false,
        cookies: { forward: "none" },
      },
      viewerProtocolPolicy: "redirect-to-https",
      minTtl: 0,
      defaultTtl: 3600,
      maxTtl: 86400,
    },
    priceClass: "PriceClass_100",
    restrictions: {
      geoRestriction: {
        restrictionType: "none",
      },
    },
    viewerCertificate: {
      cloudfrontDefaultCertificate: true,
    },
  },
  {
    dependsOn: [originAccessIdentity, bucketPolicy],
  }
);

export const oaiPath = originAccessIdentity.cloudfrontAccessIdentityPath;
export const oaiArn = pulumi.interpolate`arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity ${originAccessIdentity.id}`;
export const bucketArn = siteBucket.arn;
